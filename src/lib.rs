#![feature(test)]

extern crate itertools;
extern crate test;

use std::mem::size_of;

mod iterators;

// Originally contributed by /u/rushmorem, modified to make a separate `fn`.
// This function is actually incorrect with respect to at least one of the the UTF-8 cases below. Oops!
fn naive_iterator(string: &str) -> Vec<String> {
    use itertools::Itertools;

    let num_chars = string.chars().count(); // Renamed to `num_chars` because `len` means something different in the context of `String`s. :)
    string
        .to_lowercase()
        .chars()
        .interleave(string.to_uppercase().chars())
        .combinations(num_chars)
        .map(|s| -> String { s.into_iter().collect() })
        .filter(|s| s.to_lowercase() == string.to_lowercase())
        .unique()
        .collect()
}

// Originally contributed by /u/K900_, modified a bit to be comparable to the next iterations.
// The key insight here is that we can model all unique permutations of 1 and 0 by using integers.
fn bitmasks(string: &str) -> Vec<String> {
    if string.is_empty() { // This check necessary -- otherwise `bitmasks("")` returns `vec![""]`
        return Vec::new();
    }

    let num_chars = string.chars().count(); // USUALLY
    assert!(num_chars < size_of::<usize>() * 8, "too many characters");

    let mut cases = Vec::new();

    // I changed this from `u64` to `usize` because if we overflow `usize` with a `push(...)` then
    // we panic anyway.
    for i in 0..usize::pow(2, num_chars as u32) { // `pow` has a limitation we'll explore later.
        let mut s = String::new();
        for (idx, ch) in string.chars().enumerate() {
            if (i & (1 << idx)) == 0 {
                s.push_str(&ch.to_lowercase().to_string())
            } else {
                s.push_str(&ch.to_uppercase().to_string())
            }
        }
        cases.push(s);
    }

    cases
}

// This is the point where /u/erichdongubler started to have way too much fun. :)

// Tweaked the above a bit to pre-allocate cases, case strings, and upper/lower variants.
fn bitmasks_preallocated(string: &str) -> Vec<String> {
    if string.is_empty() {
        return Vec::new();
    }

    let num_chars = string.chars().count();
    assert!(num_chars < size_of::<usize>() * 8, "too many characters");

    let num_cases = usize::pow(2, num_chars as u32);
    let mut cases = Vec::with_capacity(num_cases);

    let (upper, lower) = string.chars().fold(
        (Vec::with_capacity(num_chars), Vec::with_capacity(num_chars)),
        |(mut upper, mut lower), c| {
            upper.push(c.to_uppercase().to_string());
            lower.push(c.to_lowercase().to_string());
            (upper, lower)
        }
    );

    let len = string.len();
    for i in 0..num_cases {
        let mut s = String::with_capacity(len);
        for idx in 0..num_chars {
            if (i & (1 << idx)) == 0 {
                s.push_str(&lower[idx])
            } else {
                s.push_str(&upper[idx])
            }
        }
        cases.push(s);
    }

    cases
}

// Further tweaks to only compute ASCII variants and leave other UTF-8 chars alone. This is nice if
// you can add this constraint and you'd like to squeeze absolute performance out of this function.
fn bitmasks_preallocated_ascii(string: &str) -> Vec<String> {
    if string.is_empty() {
        return Vec::new();
    }

    let num_chars = string.chars().count();
    assert!(num_chars < size_of::<usize>() * 8, "too many characters");

    let num_cases = usize::pow(2, num_chars as u32);
    let mut cases = Vec::with_capacity(num_cases);

    let (upper, lower) = string.chars().fold(
        (Vec::with_capacity(num_chars), Vec::with_capacity(num_chars)),
        |(mut upper, mut lower), c| {
            upper.push(c.to_ascii_uppercase());
            lower.push(c.to_ascii_lowercase());
            (upper, lower)
        }
    );

    let len = string.len();
    for i in 0..num_cases {
        let mut s = String::with_capacity(len);
        for idx in 0..num_chars {
            if (i & (1 << idx)) == 0 {
                s.push(lower[idx])
            } else {
                s.push(upper[idx])
            }
        }
        cases.push(s);
    }

    cases
}

// This is usually just about as fast as the above. However, by treating the calculating the
// bitmask a bit differently we can gain the topmost word bit back for usage.
// This change can also be appleid to the last UTF-8 version above.
fn bitmasks_preallocated_ascii_1morebit(string: &str) -> Vec<String> {
    if string.is_empty() {
        return Vec::new();
    }

    let num_chars = string.chars().count();
    assert!(num_chars <= size_of::<usize>() * 8, "too many characters");

    // Instead of using `pow` (which is intuitive to humans!), let's just generate a mask of all 1s
    // and then shift it to match the count of 1s with the number of characters.
    let max_permutation_mask = usize::max_value()
        .checked_shr(size_of::<usize>() as u32 * 8 - num_chars as u32)
        .unwrap();
    let mut cases = Vec::with_capacity(max_permutation_mask);

    let (upper, lower) = string.chars().fold(
        (Vec::with_capacity(num_chars), Vec::with_capacity(num_chars)),
        |(mut upper, mut lower), c| {
            upper.push(c.to_ascii_uppercase());
            lower.push(c.to_ascii_lowercase());
            (upper, lower)
        }
    );

    let len = string.len();
    for permutation_mask in 0..=max_permutation_mask {
        let mut s = String::with_capacity(len);
        for idx in 0..num_chars {
            if (permutation_mask & (1 << idx)) == 0 {
                s.push(lower[idx])
            } else {
                s.push(upper[idx])
            }
        }
        cases.push(s);
    }

    cases
}

fn with_iterator(string: &str) -> Vec<String> {
    if string.len() == 0 {
        vec![]
    } else {
        let cases = iterators::Cases::new(string);
        let v = cases.iter().map(|c| c.to_string()).collect();
        v
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    fn assert_sorted_eq<F: Fn(&str) -> Vec<String>>(f: &F, input: &str, expected: &[&str]) {
        let mut actual = f(input);
        actual.sort();
        let expected: Vec<_> = expected.iter().map(|s| s.to_owned()).collect();
        assert_eq!(actual, expected);
    }

    fn assert_ascii_cases_correct<F: Fn(&str) -> Vec<String>>(f: &F) {
        assert_sorted_eq(f, "", &[]);
        assert_sorted_eq(f, "a", &["A", "a"]);
        assert_sorted_eq(f, "A", &["A", "a"]);
        assert_sorted_eq(f, "foobar", &[
            "FOOBAR",
            "FOOBAr",
            "FOOBaR",
            "FOOBar",
            "FOObAR",
            "FOObAr",
            "FOObaR",
            "FOObar",
            "FOoBAR",
            "FOoBAr",
            "FOoBaR",
            "FOoBar",
            "FOobAR",
            "FOobAr",
            "FOobaR",
            "FOobar",
            "FoOBAR",
            "FoOBAr",
            "FoOBaR",
            "FoOBar",
            "FoObAR",
            "FoObAr",
            "FoObaR",
            "FoObar",
            "FooBAR",
            "FooBAr",
            "FooBaR",
            "FooBar",
            "FoobAR",
            "FoobAr",
            "FoobaR",
            "Foobar",
            "fOOBAR",
            "fOOBAr",
            "fOOBaR",
            "fOOBar",
            "fOObAR",
            "fOObAr",
            "fOObaR",
            "fOObar",
            "fOoBAR",
            "fOoBAr",
            "fOoBaR",
            "fOoBar",
            "fOobAR",
            "fOobAr",
            "fOobaR",
            "fOobar",
            "foOBAR",
            "foOBAr",
            "foOBaR",
            "foOBar",
            "foObAR",
            "foObAr",
            "foObaR",
            "foObar",
            "fooBAR",
            "fooBAr",
            "fooBaR",
            "fooBar",
            "foobAR",
            "foobAr",
            "foobaR",
            "foobar",
        ]);
    }

    fn assert_utf8_cases_correct<F: Fn(&str) -> Vec<String>>(f: &F) {
        assert_ascii_cases_correct(f);
        assert_sorted_eq(f, "Straße", &[
            "STRASSE",
            "STRASSe",
            "STRAßE",
            "STRAße",
            "STRaSSE",
            "STRaSSe",
            "STRaßE",
            "STRaße",
            "STrASSE",
            "STrASSe",
            "STrAßE",
            "STrAße",
            "STraSSE",
            "STraSSe",
            "STraßE",
            "STraße",
            "StRASSE",
            "StRASSe",
            "StRAßE",
            "StRAße",
            "StRaSSE",
            "StRaSSe",
            "StRaßE",
            "StRaße",
            "StrASSE",
            "StrASSe",
            "StrAßE",
            "StrAße",
            "StraSSE",
            "StraSSe",
            "StraßE",
            "Straße",
            "sTRASSE",
            "sTRASSe",
            "sTRAßE",
            "sTRAße",
            "sTRaSSE",
            "sTRaSSe",
            "sTRaßE",
            "sTRaße",
            "sTrASSE",
            "sTrASSe",
            "sTrAßE",
            "sTrAße",
            "sTraSSE",
            "sTraSSe",
            "sTraßE",
            "sTraße",
            "stRASSE",
            "stRASSe",
            "stRAßE",
            "stRAße",
            "stRaSSE",
            "stRaSSe",
            "stRaßE",
            "stRaße",
            "strASSE",
            "strASSe",
            "strAßE",
            "strAße",
            "straSSE",
            "straSSe",
            "straßE",
            "straße"
        ]);
    }

    #[test]
    fn test_with_iterator() {
       assert_utf8_cases_correct(&with_iterator);
    }

    #[bench]
    fn bench_with_iterator(b: &mut Bencher) {
        b.iter(|| with_iterator("foobar"))
    }


    #[test]
    fn test_naive_iterator() {
       assert_utf8_cases_correct(&naive_iterator);
    }


    #[bench]
    fn bench_naive_iterator(b: &mut Bencher) {
        b.iter(|| naive_iterator("foobar"))
    }

    #[test]
    fn test_bitmasks() {
       assert_utf8_cases_correct(&bitmasks)
    }

    #[bench]
    fn bench_bitmasks(b: &mut Bencher) {
        b.iter(|| bitmasks("foobar"))
    }

    #[test]
    fn test_bitmasks_preallocated() {
       assert_utf8_cases_correct(&bitmasks_preallocated)
    }

    #[bench]
    fn bench_bitmasks_preallocated(b: &mut Bencher) {
        b.iter(|| bitmasks_preallocated("foobar"))
    }

    #[test]
    fn test_bitmasks_preallocated_ascii() {
       assert_ascii_cases_correct(&bitmasks_preallocated_ascii)
    }

    #[bench]
    fn bench_bitmasks_preallocated_ascii(b: &mut Bencher) {
        b.iter(|| bitmasks_preallocated_ascii("foobar"))
    }

    #[test]
    fn test_bitmasks_preallocated_ascii_1morebit() {
       assert_ascii_cases_correct(&bitmasks_preallocated_ascii_1morebit)
    }

    #[bench]
    fn bench_bitmasks_preallocated_ascii_1morebit(b: &mut Bencher) {
        b.iter(|| bitmasks_preallocated_ascii_1morebit("foobar"))
    }
}
