use std::{
    usize,
    fmt::{self, Formatter, Display, Debug, Write},
};

pub struct Case<'a> {
    case_bits: u32,
    cases: &'a Cases,
}

impl<'a> Case<'a> {
    fn new(n: u32, cases: &'a Cases) -> Case<'a> {
        Case { case_bits: n, cases }
    }

    pub fn write_to<W: Write>(&self, writer: &mut W) -> Result<(), fmt::Error> {
        if self.cases.len > 0 {
            let mut bit_mask = 1 << (self.cases.len - 1);
            for i in 0 .. self.cases.len {
                if self.case_bits & bit_mask == 0 {
                    writer.write_str(&self.cases.lower[i])?;
                } else {
                    writer.write_str(&self.cases.upper[i])?;
                }
                bit_mask >>= 1;
            }
        }
        Ok(())
    }

    pub fn to_string(&self) -> String {
        let mut s = String::with_capacity(self.cases.max_capacity);
        self.write_to(&mut s).expect("Error converting to string!");
        s
    }
}

impl<'a> Display for Case<'a> {
    fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> {
        self.write_to(f)
    }
}

impl<'a> Debug for Case<'a> {
    fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> {
        write!(f, "Case {{ n = {:b}", self.case_bits)?;
        f.write_str(", text = ")?;
        Display::fmt(self, f)?;
        f.write_str(" }")?;
        Ok(())
    }
}

#[derive(Debug)]
pub struct Cases {
    upper: Vec<String>,
    lower: Vec<String>,
    len: usize,
    max: u32,
    max_capacity: usize,
}

impl Cases {
    pub fn new(input: &str) -> Cases {
        let char_strings = Cases::chars_to_strings(&input);
        let mut upper: Vec<String> = char_strings.iter().map(|s| s.to_uppercase()).collect();
        upper.shrink_to_fit();
        let mut lower: Vec<String> = char_strings.iter().map(|s| s.to_lowercase()).collect();
        lower.shrink_to_fit();
        let len = upper.len();
        let max = (1 << len) - 1;
        let max_capacity = usize::max(
                upper.iter().map(|v| v.len()).sum(),
                lower.iter().map(|v| v.len()).sum());
        Cases { upper, lower, len, max, max_capacity }
    }

    fn chars_to_strings(s: &str) -> Vec<String> {
        let mut v = Vec::with_capacity(s.len());
        let mut prev = 0;
        for i in 1 ..= s.len() {
            if s.is_char_boundary(i) {
                v.push(s[prev .. i].to_owned());    
                prev = i;
            }
        }
        v.shrink_to_fit();
        v
    }

    pub fn iter(&self) -> impl Iterator<Item = Case> {
        (0 ..= self.max)
            .map(move |i| Case::new(i, &self))
    }
}